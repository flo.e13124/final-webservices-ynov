from datetime import datetime

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask

from Controller import Database, Cinema, Movie, Reservation, Room, Sceance
from Util.Reorder import reorder_reservations

app = Flask(__name__)
app.register_blueprint(Cinema.route_blueprint)
app.register_blueprint(Movie.route_blueprint)
app.register_blueprint(Reservation.route_blueprint)
app.register_blueprint(Room.route_blueprint)
app.register_blueprint(Sceance.route_blueprint)


@app.route("/")
def index():
	return {"status": "API is running"}


@app.route("/fixture")
def setFixture():
	Database.fixture()
	return {"status": "ok"}
	
	
def verify_reservations():
	print("Verify reservations")
	sql = "SELECT uid, `rank`, status, createdAt, updateAt, expiresAt FROM reservation WHERE status = 1 ORDER BY `rank` LIMIT 1"
	data = Database.request(sql)
	if data is None or len(data) == 0:
		return
	
	if data[0][5] < datetime.utcnow():
		sql = "UPDATE reservation SET status = 3 WHERE uid = %s"
		Database.request(sql, data[0][0])
		reorder_reservations()
	
	
scheduler = BackgroundScheduler()
scheduler.add_job(verify_reservations, 'interval', minutes=5)
scheduler.start()

if __name__ == "__main__":
	Database.fixture()
	app.run(host="0.0.0.0", port=5000, debug=True)
	