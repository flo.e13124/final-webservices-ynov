# Quickstart

To retrieve docker-compose file and run it:

```git clone https://gitlab.com/flo.e13124/final-webservices-ynov; cd final-webservices-ynov; docker compose up -d```

# Launching the project

To launch the app

```pip install -r requirements.txt```

```python app.py```

```http://localhost:5000/fixture``` to generate fixtures

```http://localhost:5000/``` to access the app