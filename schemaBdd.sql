-- DELETE ALL TABLES
SET
    FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS films;
DROP TABLE IF EXISTS film;
DROP TABLE IF EXISTS categorie;
DROP TABLE IF EXISTS film_categorie;
DROP TABLE IF EXISTS affiche;
DROP TABLE IF EXISTS cinema;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS sceance;
DROP TABLE IF EXISTS movie;
SET
    FOREIGN_KEY_CHECKS = 1;

-- CREATE TABLES

CREATE TABLE movie
(
    id          INT           NOT NULL AUTO_INCREMENT,
    uid         VARCHAR(128)  NOT NULL UNIQUE,
    name        VARCHAR(128)  NOT NULL,
    description VARCHAR(4096) not null,
    duration    FLOAT         not null,
    rate        INT           not null,
    createdAt   DATETIME      NOT NULL,
    updatedAt   DATETIME      NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE cinema
(
    id       INT          NOT NULL AUTO_INCREMENT,
    uid      VARCHAR(128) NOT NULL,
    name     VARCHAR(128) NOT NULL,
    createAt DATETIME     NOT NULL,
    updateAt DATETIME     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE room
(
    id         INT          NOT NULL AUTO_INCREMENT,
    uid        VARCHAR(128) NOT NULL,
    seats      INT          NOT NULL,
    name       VARCHAR(128) NOT NULL,
    cinema_uid VARCHAR(128) NOT NULL,
    createAt   DATETIME     NOT NULL,
    updateAt   DATETIME     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE reservation
(
    id         INT          NOT NULL AUTO_INCREMENT,
    uid        VARCHAR(128) NOT NULL,
    `rank`     INT          NOT NULL,
    status     INT          NOT NULL,
    seats      INT          NOT NULL,
    seance_uid VARCHAR(128) NOT NULL,
    createdAt  DATETIME     not null,
    updateAt   DATETIME     not null,
    expiresAt  DATETIME     not null,
    PRIMARY KEY (id)
);

CREATE TABLE sceance
(
    id        INT          NOT NULL AUTO_INCREMENT,
    uid       VARCHAR(128) NOT NULL,
    movie_uid VARCHAR(128) NOT NULL,
    room_uid  VARCHAR(128) NOT NULL,
    date      DATETIME     NOT NULL,
    PRIMARY KEY (id)
);