from Controller import Database

def reorder_reservations():
	sql = "SELECT uid, `rank`, status, createdAt, updateAt, expiresAt FROM reservation WHERE status = 1 ORDER BY `rank`"
	data = Database.request(sql)
	if data is None or len(data) == 0:
		return
	
	if data[0][1] != 0:
		for reservation in data:
			sql = "UPDATE reservation SET `rank` = %s, expiresAt = NOW() + INTERVAL 5 MINUTE WHERE uid = %s"
			Database.request(sql, reservation[1] - 1, reservation[0])
			