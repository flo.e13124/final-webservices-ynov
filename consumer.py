import pika, json, sys, os
import uuid

from Controller import Database
# from main import Product, db
import time
time.sleep(20)
def main():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host='rabbitmq',
            port=5672,
            retry_delay=2, 
            credentials=pika.PlainCredentials(
                username='ynov',
                password='Ynov_Password123!'
            )
        )
    )

    channel = connection.channel()


    def callback(ch, method, properties, body):
        print('Message received')
        data = json.loads(body)
        print(data)

        if properties.content_type == 'post_rooms':
            name = data["name"]
            seats = data["seats"]
            cinemauid = data["cinemauid"]
            
            if "uid" not in data or data["uid"] is None:
                uid = str(uuid.uuid4())
            else:
                uid = data["uid"]
            if name is None or seats is None or cinemauid is None:
                print("Parameters Error")
            sql = "INSERT INTO room (name, seats, uid, cinema_uid, createAt, updateAt) VALUES (%s, %s, %s, %s, NOW(), NOW())"
            data = Database.request(sql, name, seats, uid, cinemauid)
            if data is None or len(data) == 0:
                print('Parameters Error')
            print('Create room success')

        # elif properties.content_type == 'product_updated':
        #     product = Product.query.get(data['id'])
        #     product.title = data['title']
        #     product.image = data['image']
        #     db.session.commit()
        #     print('Product Updated')

        # elif properties.content_type == 'product_deleted':
        #     product = Product.query.get(data)
        #     db.session.delete(product)
        #     db.session.commit()
        #     print('Product Deleted')

    channel.queue_declare(queue='rooms')
    channel.basic_consume(queue='rooms', on_message_callback=callback, auto_ack=True)

    print('Started Consuming')

    channel.start_consuming()

    channel.close()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)