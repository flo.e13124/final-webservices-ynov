import uuid

from flask import request, Blueprint

from Controller import Database
from Util.API import returnAPIFormat
from Controller.producer import publish

route_blueprint = Blueprint("room", __name__)


@route_blueprint.route("/cinema/<cinemauid>/rooms", methods=["GET"])
def getRooms(cinemauid):
	if cinemauid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error11")
	
	sql = "SELECT uid, seats, name, createAt, updateAt FROM room WHERE cinema_uid = %s"
	data = Database.request(sql, cinemauid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error22")
	
	formatData = []
	for i in data:
		formatData.append({
			"uid": i[0],
			"seats": i[1],
			"name": i[2],
			"createAt": i[3],
			"updateAt": i[4]
		})
	
	return returnAPIFormat(data=formatData, link=request.path, method=request.method, status=200, message="List room success")


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>", methods=["GET"])
def getRoomByUid(cinemauid, roomuid):
	if cinemauid is None or roomuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid, seats, name, createAt, updateAt FROM room WHERE cinema_uid = %s AND uid = %s"
	data = Database.request(sql, cinemauid, roomuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	formatData = []
	for i in data:
		formatData.append({
			"uid": i[0],
			"seats": i[1],
			"name": i[2],
			"createAt": i[3],
			"updateAt": i[4]
		})
	
	return returnAPIFormat(data=formatData, link=request.path, method=request.method, status=200, message="List room success")


@route_blueprint.route("/cinema/<cinemauid>/rooms", methods=["POST"])
def postRoom(cinemauid):
	data = request.get_json()
	data["cinemauid"] = cinemauid
	# name = data["name"]
	# seats = data["seats"]
	# if "uid" not in data or data["uid"] is None:
	# 	uid = str(uuid.uuid4())
	# else:
	# 	uid = data["uid"]
	
	# if name is None or seats is None or cinemauid is None:
	# 	return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	publish('post_rooms', data, 'rooms')

	# sql = "INSERT INTO room (name, seats, uid, cinema_uid, createAt, updateAt) VALUES (%s, %s, %s, %s, NOW(), NOW())"
	# data = Database.request(sql, name, seats, uid, cinemauid)
	# if data is None or len(data) == 0:
	# 	return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=200, message="message sent to rooms queue")


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>", methods=["PUT"])
def putRoom(cinemauid, roomuid):
	data = request.get_json()
	name = data["name"]
	seats = data["seats"]
	
	if name is None or seats is None or cinemauid is None or roomuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "UPDATE room SET name = %s, seats = %s, updateAt = NOW() WHERE cinema_uid = %s AND uid = %s"
	data = Database.request(sql, name, seats, cinemauid, roomuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=200, message="Update room success")


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>", methods=["DELETE"])
def deleteRoom(cinemauid, roomuid):
	if cinemauid is None or roomuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "DELETE FROM room WHERE cinema_uid = %s AND uid = %s"
	data = Database.request(sql, cinemauid, roomuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=200, message="Delete room success")