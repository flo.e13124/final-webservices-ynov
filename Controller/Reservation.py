import uuid
from datetime import datetime

from flask import request, Blueprint

from Controller import Database
from Util.API import returnAPIFormat
from Util.Reorder import reorder_reservations

route_blueprint = Blueprint("reservation", __name__)


@route_blueprint.route("/movie/<movieuid>/reservations", methods=["GET"])
def getReservationByMovie(movieuid):
	if movieuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid FROM sceance WHERE movie_uid = %s"
	data = Database.request(sql, movieuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	reservations = []
	for sceance in data:
		sql = "SELECT uid, `rank`, status, createdAt, updateAt, expiresAt FROM reservation WHERE seance_uid = %s"
		dataReservation = Database.request(sql, sceance[0])
		if dataReservation is None or len(dataReservation) == 0:
			return returnAPIFormat(data=None, link=request.path, method=request.method, status=500, message="SQL Error")
		for data in dataReservation:
			fomatData = {
				"uid": data[0],
				"rank": data[1],
				"status": data[2],
				"createdAt": data[3],
				"updateAt": data[4],
				"expiresAt": data[5]
			}
			reservations.append(fomatData)
	
	return returnAPIFormat(data=reservations, link=request.path, method=request.method, status=200, message="Reservation list")


@route_blueprint.route("/movie/<movieuid>/reservations", methods=["POST"])
def createReservation(movieuid):
	data = request.get_json()
	sceance = data["sceance"]
	nbSeats = data["nbSeats"]
	room = data["room"]
	
	if sceance is None or nbSeats is None or room is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	if nbSeats <= 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid FROM sceance WHERE uid = %s"
	data = Database.request(sql, sceance)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT seats FROM reservation WHERE seance_uid = %s AND status != 3"
	data = Database.request(sql, sceance)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	seats = 0
	for i in data:
		seats += i[0]
	
	sql = "SELECT seats FROM room WHERE uid = %s"
	data = Database.request(sql, room)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	if seats + nbSeats > data[0][0]:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Not enough seats")
	
	sql = "SELECT COUNT(uid) FROM reservation WHERE seance_uid = %s AND status = 1"
	data = Database.request(sql, sceance)
	sql = "INSERT INTO reservation (uid, `rank`, status, seats, seance_uid, updateAt, createdAt, expiresAt) VALUES (%s, %s, %s, %s, %s, NOW(), NOW(), NOW() + INTERVAL 5 MINUTE)"
	data = Database.request(sql, str(uuid.uuid4()), data[0][0], 1, nbSeats, sceance)
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=201, message="Reservation created")


@route_blueprint.route("/reservations/<uid>", methods=["GET"])
def getReservationByUid(uid):
	if uid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid, `rank`, status, createdAt, updateAt, expiresAt FROM reservation WHERE uid = %s"
	data = Database.request(sql, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	formatData = {
		"uid": data[0][0],
		"rank": data[0][1],
		"status": data[0][2],
		"createdAt": data[0][3],
		"updateAt": data[0][4],
		"expiresAt": data[0][5]
	}
	
	return returnAPIFormat(data=formatData, link=request.path, method=request.method, status=200, message="list of reservation")


@route_blueprint.route("/reservations/<uid>/confirm", methods=["POST"])
def confirmReservation(uid):
	if uid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid, `rank`, status, createdAt, updateAt, expiresAt FROM reservation WHERE uid = %s AND status = 1"
	data = Database.request(sql, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	if data[0][1] != 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="It's not your turn")
	
	if data[0][5] < datetime.utcnow():
		sql = "UPDATE reservation SET status = 3 WHERE uid = %s"
		Database.request(sql, uid)
		reorder_reservations()
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=410, message="Reservation expired")
	
	sql = "UPDATE reservation SET status = 2 WHERE uid = %s"
	Database.request(sql, uid)
	reorder_reservations()
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=201, message="Reservation confirmed")
