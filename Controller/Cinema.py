import uuid

from flask import jsonify, request, Blueprint

from Controller import Database
from Util.API import returnAPIFormat


route_blueprint = Blueprint("cinema", __name__)


@route_blueprint.route("/cinema", methods=["GET"])
def getCinema():
	sql = "SELECT uid, name, createAt, updateAt FROM cinema"
	data = Database.request(sql)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=204, message="No Content")
	
	formatdata = []
	for i in data:
		formatdata.append({
			"uid": i[0],
			"name": i[1],
			"createAt": i[2],
			"updateAt": i[3]
		})
	return returnAPIFormat(data=formatdata, link=request.path, method=request.method, status=200, message="List cinema success")


@route_blueprint.route("/cinema/<uid>", methods=["GET"])
def getCinemaByUid(uid):
	if uid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid, name, createAt, updateAt FROM cinema WHERE uid = %s"
	data = Database.request(sql, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	formatdata = []
	for i in data:
		formatdata.append({
			"uid": i[0],
			"name": i[1],
			"createAt": i[2],
			"updateAt": i[3]
		})
	return returnAPIFormat(data=formatdata, link=request.path, method=request.method, status=200, message="list cinema success")


@route_blueprint.route("/cinema", methods=["POST"])
def postCinema():
	data = request.get_json()
	name = data["name"]
	if "uid" not in data or data["uid"] is None:
		uid = str(uuid.uuid4())
	else:
		uid = data["uid"]
	
	if name is None or len(name) > 128:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "INSERT INTO cinema (name, uid, createAt, updateAt) VALUES (%s, %s, NOW(), NOW())"
	data = Database.request(sql, name, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=201, message="Create cinema success")


@route_blueprint.route("/cinema/<uid>", methods=["PUT"])
def putCinema(uid):
	data = request.get_json()
	name = data["name"]
	if uid is None or name is None or len(name) > 128:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "UPDATE cinema SET name = %s, updateAt = NOW() WHERE uid = %s"
	data = Database.request(sql, name, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=201, message="Update cinema success")


@route_blueprint.route("/cinema/<uid>", methods=["DELETE"])
def deleteCinema(uid):
	if uid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "DELETE FROM cinema WHERE uid = %s"
	data = Database.request(sql, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=404, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=204, message="Delete cinema success")
