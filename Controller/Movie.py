import uuid
from flask import request, Blueprint

from Controller import Database
from Util.API import returnAPIFormat

route_blueprint = Blueprint('film', __name__)


@route_blueprint.route("/movies/<uid>", methods=["GET"])
def getFilm(uid):
	if uid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid, name, description, rate, duration, createdAt, updatedAt FROM movie WHERE uid = %s"
	data = Database.request(sql, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="SQL Error")
	
	formatData = []
	for i in data:
		formatData.append({
			"uid": i[0],
			"name": i[1],
			"description": i[2],
			"rate": i[3],
			"duration": i[4],
			"createdAt": i[5],
			"updatedAt": i[6]
		})
		
	return returnAPIFormat(data=formatData, link=request.path, method=request.method, status=200, message="Movie found")


@route_blueprint.route("/movies", methods=["GET"])
def getListFilm():
	sql = "SELECT uid, name, description, rate, duration, createdAt, updatedAt FROM movie"
	data = Database.request(sql)
	if data is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	formatData = []
	for i in data:
		formatData.append({
			"uid": i[0],
			"name": i[1],
			"description": i[2],
			"rate": i[3],
			"duration": i[4],
			"createdAt": i[5],
			"updatedAt": i[6]
		})
	
	return returnAPIFormat(data=formatData, link=request.path, method=request.method, status=200, message="List movies")


@route_blueprint.route("/movies", methods=["POST"])
def postFilm():
	data = request.get_json()
	if "uid" not in data or data["uid"] is None:
		uid = str(uuid.uuid4())
	else:
		uid = data["uid"]
	name = data["name"]
	description = data["description"]
	rate = data["rate"]
	duration = data["duration"]
	
	if name is None or description is None or rate is None or duration is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	if rate < 0 or rate > 5 or duration < 0 or duration > 240 or len(name) > 128 or len(description) > 4096:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "INSERT INTO movie (uid, name, description, rate, duration, createdAt, updatedAt) VALUES (%s, %s, %s, %s, %s, NOW(), NOW())"
	data = Database.request(sql, uid, name, description, rate, duration)
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=201, message="Movie created")


@route_blueprint.route("/movies/<uid>", methods=["PUT"])
def Film_Categeorie(uid):
	data = request.get_json()
	name = data["name"]
	description = data["description"]
	rate = data["rate"]
	duration = data["duration"]
	if uid is None or name is None or description is None or rate is None or duration is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	if name is None or description is None or rate is None or duration is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	if rate < 0 or rate > 5 or duration < 0 or duration > 240 or len(name) > 128 or len(description) > 4096:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid FROM movie WHERE uid = %s"
	data = Database.request(sql, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=404 , message="Movie not found")
	
	sql = "UPDATE movie SET uid = %s, name = %s, description = %s, rate = %s, duration = %s, updatedAt = NOW() WHERE uid = %s"
	Database.request(sql, uid)
	
	return returnAPIFormat(data=None, link=request.path, method=request.method, status=200, message="Movie updated")


@route_blueprint.route("/movies/<uid>", methods=["DELETE"])
def deleteFilm(uid):
	if uid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT uid FROM movie WHERE uid = %s"
	data = Database.request(sql, uid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=404 , message="Movie not found")
	
	sql = "DELETE FROM movie WHERE uid = %s"
	Database.request(sql, uid)
	
	return returnAPIFormat(data=None, link=request.path, method=request.method, status=204, message="Movie deleted")
