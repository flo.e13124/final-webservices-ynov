import pika, json
import time
time.sleep(20)

connection = pika.BlockingConnection(
    pika.ConnectionParameters(
        host='rabbitmq',
        port=5672,
        retry_delay=2, 
        credentials=pika.PlainCredentials(
            username='ynov',
            password='Ynov_Password123!'
        )
    )
)

channel = connection.channel()


def publish(method, body, queue):
    properties = pika.BasicProperties(method)
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='', routing_key=queue, body=json.dumps(body), properties=properties)