import mysql.connector
import time


def create_connection():
	while True:
		try:
			conn = mysql.connector.connect(
				host='mysql',
				port=3306,
				user='root',
				password='YnovMysql',
				database='Ynov_Webservices'
			)
			return conn
		except mysql.connector.Error as err:
			print(f"Error: {err}")
			print("Retrying in 1 second...")
			time.sleep(1)


def fixture():
	bdd = create_connection()
	fd = open("schemaBdd.sql", 'r')
	sql_queries = fd.read().split(';')  # Diviser le contenu du fichier en requêtes individuelles
	fd.close()
	cursor = bdd.cursor()
	for query in sql_queries:
		print(query)
		cursor.execute(query)  # Exécuter chaque requête séparément
	bdd.close()


def request(sql, *params):
	bdd = create_connection()
	cursor = bdd.cursor()
	try:
		cursor.execute(sql, params)
		rows = cursor.fetchall()
		bdd.commit()
		bdd.close()
		return rows
	except mysql.connector.Error as err:
		print(err)
		return None
