import uuid

from flask import request, Blueprint

from Controller import Database
from Util.API import returnAPIFormat

route_blueprint = Blueprint("sceance", __name__)


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>/sceances", methods=["GET"])
def getSceance(cinemauid, roomuid):
	if cinemauid is None or roomuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT sceance.uid, date, movie.titre FROM sceance INNER JOIN room ON room.uid = sceance.room_uid INNER JOIN movie ON movie.uid = sceance.movie_uid WHERE room.uid = %s AND room.cinema_uid = %s"
	data = Database.request(sql, roomuid, cinemauid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	formatData = []
	for i in data:
		formatData.append({
			"uid": i[0],
			"date": i[1],
			"movie": i[2]
		})
	
	return returnAPIFormat(data=formatData, link=request.path, method=request.method, status=200, message="List sceances success")


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>/sceances/<sceanceuid>", methods=["GET"])
def getSceanceByUid(cinemauid, roomuid, sceanceuid):
	if cinemauid is None or roomuid is None or sceanceuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "SELECT sceance.uid, date, movie.titre FROM sceance INNER JOIN room ON room.uid = sceance.room_uid INNER JOIN movie ON movie.uid = sceance.movie_uid WHERE room.uid = %s AND room.cinema_uid = %s AND sceance.uid = %s"
	data = Database.request(sql, roomuid, cinemauid, sceanceuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	formatData = []
	for i in data:
		formatData.append({
			"uid": i[0],
			"date": i[1],
			"movie": i[2]
		})
	
	return returnAPIFormat(data=formatData, link=request.path, method=request.method, status=200, message="List sceances success")


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>/sceances", methods=["POST"])
def postSceance(cinemauid, roomuid):
	data = request.get_json()
	date = data["date"]
	movieuid = data["movieuid"]
	if "uid" not in data or data["uid"] is None:
		uid = str(uuid.uuid4())
	else:
		uid = data["uid"]
	
	if date is None or movieuid is None or cinemauid is None or roomuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "INSERT INTO sceance (uid, date, movie_uid, room_uid) VALUES (%s, %s, %s, %s)"
	data = Database.request(sql, uid, date, movieuid, roomuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=201, message="Create sceance success")


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>/sceances/<sceanceuid>", methods=["PUT"])
def putSceance(cinemauid, roomuid, sceanceuid):
	data = request.get_json()
	date = data["date"]
	movieuid = data["movieuid"]
	
	if date is None or movieuid is None or cinemauid is None or roomuid is None or sceanceuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "UPDATE sceance SET date = %s, movie_uid = %s WHERE uid = %s"
	data = Database.request(sql, date, movieuid, sceanceuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=200, message="Update sceance success")


@route_blueprint.route("/cinema/<cinemauid>/rooms/<roomuid>/sceances/<sceanceuid>", methods=["DELETE"])
def deleteSceance(cinemauid, roomuid, sceanceuid):
	if cinemauid is None or roomuid is None or sceanceuid is None:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	sql = "DELETE FROM sceance WHERE uid = %s"
	data = Database.request(sql, sceanceuid)
	if data is None or len(data) == 0:
		return returnAPIFormat(data=None, link=request.path, method=request.method, status=422, message="Parameters Error")
	
	return returnAPIFormat(data=data, link=request.path, method=request.method, status=200, message="Delete sceance success")
